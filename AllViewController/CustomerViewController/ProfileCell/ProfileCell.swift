//
//  ProfileCell.swift
//  MIVI
//
//  Created by Shailendra on 16/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet var NameLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func Confirgure(Dic : NSDictionary) {
        let DataDic = Dic.value(forKey: "data") as! NSDictionary
        let AttributesDic  = DataDic.value(forKey: "attributes") as! NSDictionary
        let Title = AttributesDic.value(forKey: "title") as! String
        let First = AttributesDic.value(forKey: "first-name") as! String
        let Last = AttributesDic.value(forKey: "last-name") as! String
        self.NameLabel.text = "\(Title)  " + "\(First)  " + "\(Last)"
    }
}
