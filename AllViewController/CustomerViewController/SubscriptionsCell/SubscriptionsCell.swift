//
//  SubscriptionsCell.swift
//  MIVI
//
//  Created by Shailendra on 16/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import UIKit

class SubscriptionsCell: UITableViewCell {

    @IBOutlet weak var DataBalanceLabel: UILabel!
    @IBOutlet weak var ExpireDateLabel: UILabel!
    @IBOutlet weak var AutoRenewalLabel: UILabel!
    @IBOutlet weak var PrimarySubscriptionLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func Confirgure(Dic : NSDictionary) {
        
        let AttributesDic  = Dic.value(forKey: "attributes") as! NSDictionary
        let DataBalance = AttributesDic.value(forKey: "included-data-balance") as! NSNumber
        let ExpiryDate = AttributesDic.value(forKey: "expiry-date") as! String
        let AutoRenewal = AttributesDic.value(forKey: "auto-renewal") as! Bool
        let PrimarySubscription = AttributesDic.value(forKey: "primary-subscription") as! Bool
        
        self.DataBalanceLabel.text = "\(DataBalance)"
        self.ExpireDateLabel.text = "\(ExpiryDate)"
        if(AutoRenewal == true){
            self.ExpireDateLabel.text = "YES"
        }
        else{
            self.ExpireDateLabel.text = "NO"
        }
        if(PrimarySubscription == true){
            self.PrimarySubscriptionLabel.text = "YES"
        }
        else{
            self.PrimarySubscriptionLabel.text = "NO"
        }
        
    }
    
}
