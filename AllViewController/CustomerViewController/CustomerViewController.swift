//
//  CustomerViewController.swift
//  MIVI
//
//  Created by Shailendra on 15/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import UIKit

class CustomerViewController : BaseViewController,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet var CustomerTableView : UITableView!
    var Dic = NSDictionary()
    var SubscriptionsDic = NSDictionary()
    var ProductsDic = NSDictionary()
    var TitleArray : [String] = []
    var ValueArray : [String] = []
    var IncludedArray  = NSArray()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomerTableView.estimatedRowHeight = 150;
        self.CustomerTableView.rowHeight = UITableViewAutomaticDimension;
        self.CustomerTableView.register(UINib(nibName: "CustomerCell", bundle: nil), forCellReuseIdentifier: "CustomerCell")
        
        self.CustomerTableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        
        self.CustomerTableView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        
        self.CustomerTableView.register(UINib(nibName: "SubscriptionsCell", bundle: nil), forCellReuseIdentifier: "SubscriptionsCell")
        
        if let filepath = Bundle.main.path(forResource: "collection", ofType: "json") {
            do {
                let result = try String(contentsOfFile: filepath)
                self.Dic = self.convertToDictionary(text: result)
                print("Dic====\(String(describing: self.Dic))")
               
                let DataDic = Dic.value(forKey: "data") as! NSDictionary
                let AttributesDic  = DataDic.value(forKey: "attributes") as! NSDictionary
                
                self.TitleArray.append("Email ID")
                let Email = AttributesDic.value(forKey: "email-address") as! String
                self.ValueArray.append("\(Email)")
                
                self.TitleArray.append("Date of Birth")
                let Dob = AttributesDic.value(forKey: "date-of-birth") as! String
                self.ValueArray.append("\(Dob)")
               
                self.TitleArray.append("Mobile Number")
                let Mobile = AttributesDic.value(forKey: "date-of-birth") as! String
                self.ValueArray.append("\(Mobile)")
                
                self.TitleArray.append("Payment Type")
                let PaymentType = AttributesDic.value(forKey: "payment-type") as! String
                self.ValueArray.append("\(PaymentType)")
                
                self.IncludedArray = Dic.value(forKey: "included") as! NSArray
              
                self.SubscriptionsDic = IncludedArray[1] as! NSDictionary
                self.ProductsDic = IncludedArray[2] as! NSDictionary
                
            } catch {
                // contents could not be loaded
            }
        } else {

        }
    }
    
    func convertToDictionary(text: String) -> NSDictionary   {
        if let data = text.data(using: .utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                   return json
                }
            } catch  {
                let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                print("jsonStr====\(String(describing: jsonStr))")
            }
        }
        return NSDictionary()
    }
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TitleArray.count + 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let Cell : ProfileCell = tableView.dequeueReusableCell(
                withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
            Cell.Confirgure(Dic: self.Dic)
            return Cell
        }
        else if(indexPath.row == TitleArray.count + 1){
            let Cell : SubscriptionsCell = tableView.dequeueReusableCell(
                withIdentifier: "SubscriptionsCell", for: indexPath) as! SubscriptionsCell
             Cell.Confirgure(Dic: self.SubscriptionsDic)
            return Cell
        }
        else if(indexPath.row == TitleArray.count + 2){
            let Cell : ProductCell = tableView.dequeueReusableCell(
                withIdentifier: "ProductCell", for: indexPath) as! ProductCell
             Cell.Confirgure(Dic: self.ProductsDic)
            return Cell
        }
        else{
            let Cell : CustomerCell = tableView.dequeueReusableCell(
                withIdentifier: "CustomerCell", for: indexPath) as! CustomerCell
            Cell.TitleLabel.text = "\(TitleArray[indexPath.row - 1])"
            Cell.ValueLabel.text = "\(ValueArray[indexPath.row - 1])"
            return Cell
        }
      
    }
    @IBAction func BackBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
}
