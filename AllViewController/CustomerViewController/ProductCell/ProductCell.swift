//
//  ProductCell.swift
//  MIVI
//
//  Created by Shailendra on 16/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var ProductNameLabel: UILabel!
    @IBOutlet weak var ProductPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func Confirgure(Dic : NSDictionary) {
        let AttributesDic  = Dic.value(forKey: "attributes") as! NSDictionary
        let name = AttributesDic.value(forKey: "name") as! String
        let price = AttributesDic.value(forKey: "price") as! NSNumber
        
        self.ProductNameLabel.text = "\(name)"
        self.ProductPriceLabel.text = "\(price)"
    }
}
