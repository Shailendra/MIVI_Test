//
//  ViewController.swift
//  MIVI
//
//  Created by Shailendra on 15/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import UIKit

class ViewController : BaseViewController, WebServiceProtocal,UITextFieldDelegate {
    
    //MARK: - Properties
    @IBOutlet var CustomView                  : UIView!
    @IBOutlet var ShowOrHidePINBtn            : UIButton!
    @IBOutlet var MobileNoTextField           : UITextField!
    @IBOutlet var PasswordNoTextField         : UITextField!
    
    var isPINShow                             : Bool!    = false
    //end
    
    //MARK: - UIViewController life cycle
    override func viewDidLoad() {
        self.MobileNoTextField.text = "0468874507"
        self.PasswordNoTextField.text = "123456"
        super.viewDidLoad()
        self.CustomView.addShadow(cornerRadius: 10)
        self.isPINShow = false
        self.ShowOrHidePINBtn.setImage(UIImage(named:"Hide"), for: UIControlState.normal)
    }
    
    func GetValidation() -> Bool{
        if(self.MobileNoTextField.text! == ""){
            self.showAlert(Message:"\(kMobileValidation)")
            return false
        }
        else if (self.MobileNoTextField.text! != "0468874507"){
            self.showAlert(Message:"\(kMobileNoValidation)")
            return false
        }
        else if(self.PasswordNoTextField.text! == ""){
            self.showAlert(Message:"\(kPasswordValidation)")
            return false
        }
        else if(self.PasswordNoTextField.text! != "123456"){
            self.showAlert(Message:"\(kPasswordNoValidation)")
            return false
        }
        return true
    }
    
    //MARK: - UITextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
         let newLength = (textField.text?.count)! + string.count - range.length
        if(textField == self.MobileNoTextField){
            return newLength <= kMobileLimit
        }
        else if(textField == self.PasswordNoTextField){
            return newLength <= kPasswordLimit
        }
        return true
    }
    //end
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.MobileNoTextField.resignFirstResponder()
        self.PasswordNoTextField.resignFirstResponder()
        return true
    }
    @IBAction func TapGesture(){
        self.MobileNoTextField.resignFirstResponder()
        self.PasswordNoTextField.resignFirstResponder()
    }
    @IBAction func LoginBtnAction(_ sender: Any){
        if GetValidation(){
            let ObjCustomerViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomerViewController") as! CustomerViewController
            self.navigationController?.pushViewController(ObjCustomerViewController, animated: true)
        }
    }
    @IBAction func BackBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func HideShowBtnAction(_ sender: Any) {
        if self.isPINShow {
            self.PasswordNoTextField.isSecureTextEntry = true
            self.ShowOrHidePINBtn.setImage(UIImage(named:"Show"), for: UIControlState.normal)
            self.isPINShow = false
        } else {
            self.PasswordNoTextField.isSecureTextEntry = false
            self.ShowOrHidePINBtn.setImage(UIImage(named:"Hide"), for: UIControlState.normal)
            self.isPINShow = true
        }
    }
}

