//
//  ViewExtension.swift
//  MIVI
//
//  Created by Shailendra on 15/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addShadow(cornerRadius: CGFloat) {
        layer.cornerRadius = 10
        // shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 2.0
    }
}
