//
//  Constant.swift
//  MIVI
//
//  Created by Shailendra on 15/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import Foundation

let kInternetKey             = "Please check your internet connection"
let kMobileValidation        = "Please enter 10 digit mobile no."
let kMobileNoValidation      = "Please enter 0468874507 mobile no."
let kPasswordValidation      = "Please enter Password"
let kPasswordNoValidation    = "Please enter 123456 password"

let kMobileLimit             = 10
let kPasswordLimit           = 6



// Mark Web Api Param encoded
extension String {
    public func addingPercentEncodingForQueryParameter() -> String? {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$'()*+,;"
        
        var allowedCharacterSet = CharacterSet.urlQueryAllowed
        allowedCharacterSet.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        
        return addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)
    }
}
