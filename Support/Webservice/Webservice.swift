//
//  Webservice.swift
//  MIVI
//
//  Created by Shailendra on 15/08/18.
//  Copyright © 2018 Sk Info Tech. All rights reserved.
//

import Foundation

@objc protocol WebServiceProtocal{
    @objc optional func getResponse(result:NSDictionary)
    @objc optional func getArrayResponse(result:NSArray)
    @objc optional func getErrorResponse(error:NSString)
}

class WebServiceCall: NSObject {
   
    var DomainStr : String! = ""
    var delegate : WebServiceProtocal?
    
    func callService(url:String,method:String,parameter:String){
        print("url===\(url)")
        print("parameter===\(parameter)")
        
        var param = parameter
        let session = URLSession.shared
        let Url = URL(string: url)
        let request = NSMutableURLRequest(url:Url!)
        request.httpMethod = method
        request.timeoutInterval = 60
        param = param.addingPercentEncodingForQueryParameter()!
        request.httpBody = param.data(using: .utf8)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    self.mainResponse(result: json)
                } else {
                   
                    if let jsonArry = try JSONSerialization.jsonObject(with:  data!, options: []) as? NSArray {
                        self.mainAarryResponse(result: jsonArry)
                    }
                }
            } catch  {
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                self.eroorResponse(result: jsonStr!)
            }
        })
        task.resume()
    }
    
    func mainResponse(result:NSDictionary){
        DispatchQueue.main.async {
            self.delegate?.getResponse!(result: result)
        }
    }
    func eroorResponse(result:NSString){
        DispatchQueue.main.async {
            self.delegate?.getErrorResponse!(error: result)
        }
    }
    func mainAarryResponse(result:NSArray){
        DispatchQueue.main.async {
            self.delegate?.getArrayResponse!(result: result)
        }
    }
    func convertToDictionary(text: String) -> NSDictionary {
        let Dic = NSDictionary()
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            } catch {
            }
        }
        return Dic
    }
}
